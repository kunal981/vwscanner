//
//  WebviewController.h
//  Grapevine
//
//  Created by Zachary Gavin on 10/3/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//
#import <AVFoundation/AVFoundation.h>

@interface WebviewController : UIViewController <UIWebViewDelegate,NSURLConnectionDelegate> {
   UIWebView* webview;
    BOOL  isLoading,isDone;
    UIAlertView *alert;
    NSURLConnection *con;
    NSURLRequest *requestObj;
    AVMetadataObject *metaDataObj;
}

@property (nonatomic,strong) NSString* url;
@property (nonatomic,strong)AVMetadataObject *metaDataObj;

@end
