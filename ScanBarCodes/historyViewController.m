//
//  historyViewController.m
//  VWScanner
//
//  Created by brst on 3/24/15.
//  Copyright (c) 2015 Infragistics. All rights reserved.
//

#import "historyViewController.h"
#import "DBfile.h"
#import "DBManager2.h"
#import <MessageUI/MessageUI.h>

@interface historyViewController ()<UITableViewDelegate,UITableViewDataSource,MFMailComposeViewControllerDelegate,UIAlertViewDelegate>
{
    UITableView *table;
    UILabel *historyLabl;
}

@end

@implementation historyViewController
@synthesize barcodeArr;


-(void)loadView
{
    
    UIView *view = [[UIView alloc] initWithFrame:[UIScreen mainScreen].applicationFrame];
    [view setBackgroundColor:[UIColor blackColor]];
    self.view = view;
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self toplayerView];
    table=[[UITableView alloc]initWithFrame:CGRectMake(0, 60, self.view.frame.size.width, self.view.frame.size.height-60)];
    [table setSeparatorColor:[UIColor grayColor]];
    table.backgroundColor=[UIColor clearColor];
    table.delegate=self;
    table.dataSource=self;
    table.allowsMultipleSelectionDuringEditing=NO;
    table.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    [self.view addSubview:table];
    
    
    
}
-(void)viewWillAppear:(BOOL)animated
{
   
    if(barcodeArr.count==0)
    {
        table.hidden=YES;
    }
    else
    {
        table.hidden=NO;
    }
    
    historyLabl.text=@"History";
    if(barcodeArr.count>0)
    {
        historyLabl.text=[NSString stringWithFormat:@"History(%lu)",(unsigned long)barcodeArr.count];
        [table reloadData];
    }

}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return barcodeArr.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellidentifier=nil;
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellidentifier];
    if(cell==nil)
    {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellidentifier];
        
    }
    cell.backgroundColor=[UIColor clearColor];
    cell.textLabel.textColor=[UIColor grayColor];
    cell.detailTextLabel.textColor=[UIColor grayColor];
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    DBfile *dbObj=[barcodeArr objectAtIndex:indexPath.row];
    cell.textLabel.text=dbObj.barcodeUrl;
    cell.detailTextLabel.text=dbObj.barcodeUrl;
    dbObj=nil;
    

    return cell;

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}

-(void)toplayerView
{
    UIView *blueLine=[[UIView alloc]initWithFrame:CGRectMake(0, 60, self.view.frame.size.width, 1)];
    blueLine.backgroundColor=[UIColor colorWithRed:0.f green:(153/255.0f) blue:(240/255.0f) alpha:1];
    [self.view addSubview:blueLine];
    
    /*UIImageView *iconImage=[[UIImageView alloc]initWithFrame:CGRectMake(10, 20, 35, 35)];
    //iconImage.layer.cornerRadius=iconImage.frame.size.width/2;
    iconImage.image=[UIImage imageNamed:@"VW_blue_Globe_with_ring.png"];
    [self.view addSubview:iconImage];*/
    
    UIButton *backButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 25, 25, 25)];
    [backButton setImage:[UIImage imageNamed:@"backSymbol.png"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButton];
    
    
    historyLabl=[[UILabel alloc]initWithFrame:CGRectMake(0, 20, self.view.frame.size.width, 35)];
    
    historyLabl.textAlignment=NSTextAlignmentCenter;
    historyLabl.textColor=[UIColor whiteColor];
    [self.view addSubview:historyLabl];
    
    UIButton *deleteBtn=[[UIButton alloc]init];
    deleteBtn.frame=CGRectMake(self.view.frame.size.width-50, 20, 35, 35);
    [deleteBtn setImage:[UIImage imageNamed:@"delete.png"] forState:UIControlStateNormal];
    [deleteBtn addTarget:self action:@selector(deleteAllHistory) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:deleteBtn];
    
    UIButton *shareBtn=[[UIButton alloc]init];
    shareBtn.frame=CGRectMake(self.view.frame.size.width-100, 20, 35, 35);
     //shareBtn.frame=CGRectMake(self.view.frame.size.width-50, 20, 35, 35);
    [shareBtn setImage:[UIImage imageNamed:@"share.png"] forState:UIControlStateNormal];
    [shareBtn addTarget:self action:@selector(shareBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:shareBtn];
    
}
-(void)backBtnPressed
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        DBfile *obj=[barcodeArr objectAtIndex:indexPath.row];
        [[DBManager2 getSharedInstance]deleteRecord:obj.barcode_id];
        [barcodeArr removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        if(barcodeArr.count>0)
            historyLabl.text=[NSString stringWithFormat:@"History(%lu)",(unsigned long)barcodeArr.count];
        else
        {
            historyLabl.text=@"History";
            tableView.hidden=YES;
        }
    }
    
}
-(void)createCsvFile
{
    BOOL success = NO;
    
    //Creating Excel file from database recod stored in array
    
    NSMutableArray *barcodeArr1=[[NSMutableArray alloc]init];
    [barcodeArr1 addObject:@"Sr. no,Name,URL"];
    for(int i=0;i<barcodeArr.count;i++)
    {
        DBfile *obj=[barcodeArr objectAtIndex:i];
        [barcodeArr1 addObject:[NSString stringWithFormat:@"\n%d,%@,%@",(i+1),obj.name,obj.barcodeUrl]];
        obj=nil;
    }
    
    NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filename=[NSString stringWithFormat:@"barcode.csv"];
    NSString *filePathLib = [NSString stringWithFormat:@"%@",[docDir stringByAppendingPathComponent:filename]];
    NSLog(@"filepath=%@",filePathLib);
    
    [[barcodeArr1 componentsJoinedByString:@","] writeToFile:filePathLib atomically:YES encoding:NSUTF8StringEncoding error:NULL];
    
    //Sending excel file via mail
    
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
        mail.mailComposeDelegate = self;
        [mail setSubject:@" File from VWScanner App"];
        [mail setToRecipients:@[@"testingEmail@example.com"]];
        
        NSData* databaseRecord = [NSData dataWithContentsOfFile: filePathLib];
        [mail addAttachmentData:databaseRecord mimeType:@"application/octet-stream" fileName:filename];
        NSString* emailBody = @"Attached is the Database Record of barcode Scan from VWScanner App.";
        [mail setMessageBody:emailBody isHTML:YES];
        [self presentViewController:mail animated:YES completion:NULL];
        success = YES;
    }
    else
    {
        NSLog(@"This device cannot send email");
        
    }

    if (!success) {
        UIAlertView* warning = [[UIAlertView alloc] initWithTitle: @"Sorry!"
                                                          message: @"Unable to send Mail!"
                                                         delegate: nil
                                                cancelButtonTitle: @"Ok"
                                                otherButtonTitles: nil];
        [warning show];
        
    }

}

-(void)shareBtnPressed
{
    if(barcodeArr.count>0)
    {
        [self createCsvFile];
        
    }
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    //BOOL success = NO;
    NSString *msg;
    switch (result) {
        case MFMailComposeResultSent:
            msg=@"Your email is successfully sent.";
            break;
        case MFMailComposeResultSaved:
            msg=@"Your email is successfully saved to draft";
            break;
        case MFMailComposeResultCancelled:
            msg=@"You cancelled sending this email.";
            break;
        case MFMailComposeResultFailed:
            msg=@"An error occurred when trying to compose this email";
            break;
        default:
            msg=@"An error occurred when trying to compose this email";
            break;
    }
    NSLog(@"msg=%@",msg);
    [self dismissViewControllerAnimated:YES completion:NULL];
    UIAlertView* warning = [[UIAlertView alloc] initWithTitle: nil
                                                      message: msg
                                                     delegate: nil
                                            cancelButtonTitle: @"OK"
                                            otherButtonTitles: nil];
    [warning show];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}
-(void )deleteAllHistory
{
    
    UIAlertView* warning = [[UIAlertView alloc] initWithTitle: nil
                                                      message: @"Are You Sure?"
                                                     delegate: self
                                            cancelButtonTitle: @"Cancel"
                                            otherButtonTitles: @"OK",nil];
    [warning show];
    
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==1) {
        NSLog(@"Delate All=%d", [[DBManager2 getSharedInstance]deleteAll]);
        [barcodeArr removeAllObjects];
        [table reloadData];
        historyLabl.text=@"History";
       
    }
}

@end
